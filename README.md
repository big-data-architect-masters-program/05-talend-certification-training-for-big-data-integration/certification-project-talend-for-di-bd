# Certification Project: Talend For DI & BD Certification Training

## Problem Statement:

In any E-commerce website when a new product has to be added for sale, it has to adhere to many business Rules. (For multiple countries i.e., product attributes change w.r.t country Ex: Language changes as per country)

Consider a case where we have to upload a large number of new products for each country to E-commerce website. Below are the problems:

i. If a product did not adhere to any one business rule it is rejected, but product owner is not able to figure out 
for which business rule it is failing to correct the data.

ii. If product owner is correcting one error by looking at the error report and again uploads data, it may fail for next business rule i.e. all possible errors are not captured in one go.

iii. Management is not able to have a big picture of statistics i.e. for each country how many products passed and how many failed. So that they can concentrate on the country which has less data quality for product data.

## Steps and Solutions:

1. Input Files will be provided country wise i.e. each country will have separate input file.

   A. Place the input files in a folder and read each of the file iteratively (Implement the knowledge of iteration links and `tFileList` Component)

   B. Input file Structure will be as below:

   `File Name: Products_For_India.csv`

   | Product ID | Model | Country | Available QTY | Global Delivery |
   | ---------- | ----- | ------- | ------------- | --------------- |
   | 111        | 1988  | IN      | 45            | NO              |
   
   - place input files in 1 folder
   
      ![](screenshots/cert-proj-001.png)

   - use `tFileList` to access the folder and list out the files
   
      ![](screenshots/cert-proj-002.png)

   - use `tForeach` to iterate on each file and capture the file path

       ![](screenshots/cert-proj-003.png)

   - use `tFileInputExcel` to read the content of each file
   
      ![](screenshots/cert-proj-004.png)
   
   - use `tUnit` to combine the content of files in the folder
   
        ![](screenshots/cert-proj-005.png)

   - so the overall job will look like this for now:
     
        ![](screenshots/cert-proj-006.png)

   - for now, I just log into `tLogRow` and run the job as below:
    
        ![](screenshots/cert-proj-007.png)

        ![](screenshots/cert-proj-008.png)

  C. After reading the input file, lookup for Price details in Price Lookup file (Implement the knowledge of join/lookup components)   
  `File Name: Prices_For_India.csv`

  | Product ID | Country | Price per QTY | Currency |
  | --- | --- | --- | --- |
  | 111 | IN | 45 | INR |

- same approach as 1.A & 1.B, we can unite the files price detail
  - ![](screenshots/cert-proj-009.png)
  - ![](screenshots/cert-proj-010.png)
  - ![](screenshots/cert-proj-011.png)
- now we add `tJoin` to join and both dataset
  - ![](screenshots/cert-proj-012.png)
  - ![](screenshots/cert-proj-013.png)
  - ![](screenshots/cert-proj-014.png)
  - ![](screenshots/cert-proj-015.png)
- now run the job again with `tLogRow` output
  - ![](screenshots/cert-proj-016.png)
  - ![](screenshots/cert-proj-017.png)
- 

---

_to be continue..._

---

  D. Load the looked up data in MYSQL database in below structure. (Implement the knowledge of MySql components)

  `Table: Products_Staging`

  | Product ID | Model | Country | Available QTY | Price per QTY | Currency | Global Delivery |
  | --- | --- |--- |--- |--- |--- |--- |
  | 111 | 1988 | IN | 45 | 2000 | INR | NO

4. Once staging the data is ready in the MySQL database, apply the below business Rules.

   A. Product ID should be unique for each country. (i.e. same country cannot have two products with same product id,
   however same product can exist for two Countries). Reject the product if it is duplicate for same country and log error Message.
   (Implement knowledge of unique/ filter components)

   B. Reject the product if either of the below fields are NULL or empty on invalid by logging separate error message for each field.

   i. Product ID

   ii. Model
   
   iii. Country
   
   iv. Price
   
   v. Currency

   (Implement the knowledge of tMap to divide the input rows based on conditions above)

5. Error Report Format should be as below and should be loaded to Hive:
   (Implement the knowledge of HDFS components to move the generated file to HDFS and then load to Hive)

   | Product ID | Country | Business Rule | Attribute | Reason |
   | --- | --- | --- | --- | --- |
   | 1 | US | A | Product ID | Duplicate Product ID |
   | 1 | IN | B | Currency | Currency is null |

6. On the data which has successfully passed the business rules, perform the following actions:

   A. Introduce a new column OUT_OF_STOCK and populate the value for that field as TRUE if `Available QTY` from input file is 0 else populate the field as FALSE.

   B. Divide the products country wise.

   C. The final output which has be sent to Kafka will be country wise, as shown below:

   `KAFKA TOPIC: INDIA_PRODUCTS`

   | Product ID | Model | Country | Available QTY | Price Per QTY |  Currency | Global Delivery | OUT_OF_STOCK |
   | --- | --- | --- | --- | --- |  --- | --- | --- |
   | 111 | 1988 | IN | 45 | 2000 | INR | NO | FALSE |
   | 222 | 2000 | IN | 0 | 1400 | INR | YES | TRUE |

   `KAFKA TOPIC: US_PRODUCTS`

   | Product ID | Model | Country | Available QTY | Price Per QTY |  Currency | Global Delivery | OUT_OF_STOCK |
   | --- | --- | --- | --- | --- |  --- | --- | --- |
   | 111 | 1988 | US | 0 | 2000 | USD | YES | TRUE |

7. As the last step a reconciliation report is to be generated and loaded to PIG by getting details from input file 
   and aggregating the Error Report data i.e.

   A. Have count of products from input file country wise (Available products).

   B. Have count of Products passed all business Rules (Processed Products).

   C. Aggregate the error report (because error report will have more than one error per product).

   (Implement the knowledge of HDFS components to move the generated file to HDFS and then load it to PIG and later use PIG Aggregator for results).

   | Country | Total Errors | Available Products | Processed Products | Rejected Products |
   | --- | --- | --- | --- | --- |
   | US | 3 | 2 | 1 | 1 |
   | IN | 2 | 2 | 1 | 1 |

8. Once done with all the above steps, read the country wise processed data available in Kafka using Kafka input 
component and store them into HDFS as last step.
